KeepWebsitesAlive
=================

Keep websites alive by sending a HEAD request.

- Run the executable from an administrative prompt to install the scheduled task.
- Create `KeepWebsitesAlive.config` in the install directory.

The config file lists all the URLs to check. Every URL must be on a separate line and must be publicly accessible. E.g.:

```
https://github.com/
https://stackoverflow.com/
```