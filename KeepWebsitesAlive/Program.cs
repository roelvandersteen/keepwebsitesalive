﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using log4net;
using Microsoft.Win32.TaskScheduler;
using Nito.AsyncEx.Synchronous;
using Task = System.Threading.Tasks.Task;

namespace KeepWebsitesAlive
{
    internal static class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));

        [STAThread]
        public static void Main()
        {
            CreateScheduledTask();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var configFile = Path.ChangeExtension(GetAssemblyFullPath(), "config");
            var logFile = Path.ChangeExtension(configFile, ".log");
            LoggerConfiguration.Configure(logFile);

            var requestUris = GetRequestUris(configFile);

            foreach (StatusResult statusResult in CheckAllAsync(requestUris).WaitAndUnwrapException())
            {
                Logger.Info(statusResult.ToString());
            }
        }

        private static Task<StatusResult[]> CheckAllAsync(IEnumerable<string> requestUris)
        {
            var tasks = requestUris.Select(RequestHttpHeaderAsync);
            return Task.WhenAll(tasks);
        }

        private static void CreateScheduledTask()
        {
            using (var ts = new TaskService())
            {
                if (ts.AllTasks.Any(t => t.Name == "KeepWebsitesAlive"))
                {
                    return;
                }

                TaskDefinition td = ts.NewTask();
                td.RegistrationInfo.Description = "Keep websites alive by sending a HEAD request";
                td.Triggers.Add(new DailyTrigger
                {
                    StartBoundary = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Unspecified), DaysInterval = 1,
                    Repetition = new RepetitionPattern(TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(1439))
                });
                td.Actions.Add(new ExecAction(Path.ChangeExtension(GetAssemblyFullPath(), "exe")));

                ts.RootFolder.RegisterTaskDefinition(@"KeepWebsitesAlive", td);
            }
        }

        private static string GetAssemblyFullPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            return Path.GetFullPath(path);
        }

        private static IEnumerable<string> GetRequestUris(string path)
        {
            return File.ReadLines(path).Select(s => Regex.Replace(s.Trim(), @"\s.*", "")).Where(s => !string.IsNullOrEmpty(s));
        }

        private static async Task<StatusResult> RequestHttpHeaderAsync(string requestUri)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var message = new HttpRequestMessage(HttpMethod.Head, requestUri);
                    HttpResponseMessage response = await client.SendAsync(message).ConfigureAwait(false);
                    return new StatusResult {StatusCode = response.StatusCode, RequestUri = requestUri};
                }
            }
            catch (Exception e)
            {
                Logger.Error($"{requestUri} {e.Message}");
                return new StatusResult {StatusCode = HttpStatusCode.InternalServerError, RequestUri = requestUri};
            }
        }

        private class StatusResult
        {
            public string RequestUri;
            public HttpStatusCode StatusCode;

            public override string ToString()
            {
                return $"[{StatusCode}] {RequestUri}";
            }
        }
    }
}